window.addEventListener("DOMContentLoaded", function() {
  document.getElementById('faqBotButton').addEventListener('click', function(){
    this.style.display = 'none';
    document.getElementById('faqBot').style.display = 'block';
  });
  document.getElementById('faqBotClose').addEventListener('click', function(){
    document.getElementById('faqBot').style.display = 'none';
    document.getElementById('faqBotButton').style.display = 'block';
  });
}, false);