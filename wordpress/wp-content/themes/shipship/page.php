<?php get_header(); ?>

<?php if(is_page('Om ShipShip')) : ?>
  <div class="about">
    <?php $about_section_title = carbon_get_post_meta($post->ID, 'about_section_title');  ?>
    <?php if($about_section_title) : ?>
      <h1 class="about__title"><?php echo $about_section_title ?></h1>
    <?php endif; ?>
    <?php $about_section_text = carbon_get_post_meta($post->ID, 'about_section_text');  ?>
    <?php if($about_section_text) : ?>
      <h2 class="about__description"><?php echo $about_section_text ?></h2>
    <?php endif; ?>
  </div>
  <?php include('parts/contact-section.php'); ?>
<?php endif; ?>

<?php if(is_page('Kontakt')) : ?>
  <?php include('parts/contact.php'); ?>
<?php endif; ?>

<?php get_footer(); ?>