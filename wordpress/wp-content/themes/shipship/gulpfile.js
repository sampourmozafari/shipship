var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var lost = require('lost');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function() {
  return gulp.src(['src/scss/**/*.scss', '!src/scss/project-styles.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([lost(), autoprefixer()]))
    .pipe(concat('project-styles.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream())
});

gulp.task('serve', ['sass'], function() {
  browserSync.init({
    proxy: {
      target: 'http://shipship.localhost:9000'
    },
    online: true,
    port: 3000
  });
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('./**/*.php').on('change', browserSync.reload);
});

gulp.task('default', ['serve']);