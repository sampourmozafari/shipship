<div class="contact-section">
  <?php $page = get_page_by_path( 'Kontakt' ); ?>
  <a class="contact-section__wrapper" href="<?php echo get_permalink( $page ); ?>">
    <?php $contact_section_title = carbon_get_post_meta($post->ID, 'contact_section_title');  ?>
    <?php if($contact_section_title) : ?>
      <h1 class="contact-section__wrapper--title"><?php echo $contact_section_title ?> &nbsp;<i class="fas fa-external-link-alt"></i></h1>
    <?php endif; ?>
    <?php $contact_section_text = carbon_get_post_meta($post->ID, 'contact_section_text');  ?>
    <?php if($contact_section_text) : ?>
      <h2 class="contact-section__wrapper--description"><?php echo $contact_section_text ?></h2>
    <?php endif; ?>
    <?php $contact_section_cta = carbon_get_post_meta($post->ID, 'contact_section_cta');  ?>
    <?php if($contact_section_cta) : ?>
      <h3 class="contact-section__wrapper--cta" href="#"><?php echo $contact_section_cta ?></h3>
    <?php endif; ?>
  </a>
</div>