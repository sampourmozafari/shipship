<div class="header">
  <?php $frontpage_header = carbon_get_post_meta($post->ID, 'frontpage_header_image');  ?>
  <?php if($frontpage_header) : ?>
    <div class="header__img">
      <img src="<?php echo $frontpage_header ?>" alt="">
    </div>
  <?php endif; ?>
  <div class="header__content">
    <?php $frontpage_header_heading_title = carbon_get_post_meta($post->ID, 'frontpage_header_heading_title');  ?>
    <?php if($frontpage_header_heading_title) : ?>
      <h1 class="header__content--title"><?php echo $frontpage_header_heading_title ?></h1>
    <?php endif; ?>
    <?php $frontpage_header_heading_subtitle = carbon_get_post_meta($post->ID, 'frontpage_header_heading_subtitle');  ?>
    <?php if($frontpage_header_heading_subtitle) : ?>
      <h2 class="header__content--subtitle"><?php echo $frontpage_header_heading_subtitle ?></h2>
    <?php endif; ?>
    <?php $frontpage_contact_button_text = carbon_get_post_meta($post->ID, 'frontpage_contact_button_text');  ?>
    <?php if($frontpage_contact_button_text) : ?>
      <a class="header__content--cta" href=""><?php echo $frontpage_contact_button_text ?></a>
    <?php endif; ?>
  </div>
</div>