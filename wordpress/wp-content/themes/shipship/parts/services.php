<?php $services = carbon_get_post_meta($post->ID, 'services_group'); ?>

<div class="services" itemscope itemtype="http://schema.org/ItemList">
  <?php foreach ($services as $service): ?>
    <div class="service-item" itemprop="itemListElement">
      <div class="service-item__icon" itemprop="image">
        <img src="<?php echo $service['service_icon']; ?>" alt="">
      </div>
      <div class="service-item__content">
        <h6 class="service-item__content--name" itemprop="name"><?php echo $service['service_title']; ?></h6>
        <p class="service-item__content--description" itemprop="description"><?php echo $service['service_description']; ?></p>
      </div>
    </div>
  <?php endforeach; ?>
</div>