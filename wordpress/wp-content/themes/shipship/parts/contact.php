<div class="contact">
  <div class="contact__content">
    <?php $contact_page_phone = carbon_get_post_meta($post->ID, 'contact_page_phone');  ?>
    <?php if($contact_page_phone) : ?>
      <p class="contact__content--phone"><?php echo $contact_page_phone ?></p>
      <a class="contact__content--phone-cta" href="tel:+4528681560">Ring til os</a>
    <?php endif; ?>
    <?php $contact_page_email = carbon_get_post_meta($post->ID, 'contact_page_email');  ?>
    <?php if($contact_page_email) : ?>
      <p>Send os en mail og hør mere</p>
      <a class="contact__content--mail" href="mailto:<?php echo $contact_page_email ?>"><?php echo $contact_page_email ?></a>
    <?php endif; ?>
    <?php $contact_page_name = carbon_get_post_meta($post->ID, 'contact_page_name');  ?>
    <?php if($contact_page_name) : ?>
      <h3 class="contact__content--name"><?php echo $contact_page_name ?></h3>
    <?php endif; ?>
    <?php $contact_page_cvr = carbon_get_post_meta($post->ID, 'contact_page_cvr');  ?>
    <?php if($contact_page_cvr) : ?>
      <h3 class="contact__content--cvr" href="#">Cvr. nr. <?php echo $contact_page_cvr ?></h3>
    <?php endif; ?>
  </div>
  <div class="contact__form">
    <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
        <div class="contact-form">
          <?php the_content(); ?>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>
</div>