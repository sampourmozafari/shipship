<?php $integrations = carbon_get_post_meta($post->ID, 'integrations_group'); ?>

<div class="integrations" itemscope itemtype="http://schema.org/ItemList">
  <?php foreach ($integrations as $integration): ?>
    <div class="integration-item" itemprop="itemListElement">
      <div class="integration-item__logo" itemprop="image">
        <img src="<?php echo $integration['integration_logo']; ?>" alt="">
      </div>
      <h6 class="integration-item__name" itemprop="name"><?php echo $integration['integration_title']; ?></h6>
    </div>
  <?php endforeach; ?>
</div>