<div class="navigation">
  <a id="logo-icon" href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:serif="http://www.serif.com/" height="40px" viewBox="0 0 40 45" version="1.1" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
      <g>
        <path d="M19.821,2.5l-17.321,10l0,20l17.321,10l17.32,-10l0,-20l-17.32,-10Z" style="fill:none;stroke:#1b74e8;stroke-width:5px;"/>
        <path d="M2.5,12.5l17.321,10" style="fill:none;stroke:#1b74e8;stroke-width:5px;"/>
        <path d="M19.821,22.5l0,20l-8.661,-5l8.661,5l8.66,-5" style="fill:none;stroke:#253858;stroke-width:5px;"/>
      </g>
    </svg>
  </a>
  <a id="logo-text" href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">ShipShip</a>
  <nav role="navigation" class="menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <?php wp_nav_menu( array('theme_location' => 'navigation-menu') ); ?>
  </nav>
  <?php $page = get_page_by_path( 'Kontakt' ); ?>
  <a id="cta" href="<?php echo get_permalink( $page ); ?>">Kontakt Os</a>
</div>