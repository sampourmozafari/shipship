<div class="footer">
  <div class="footer__content">
    <a class="footer__content__logo" href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">ShipShip</a>
    <nav class="footer__content__menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <?php wp_nav_menu( array('theme_location' => 'footer-menu') ); ?>
    </nav>
  </div>
  <div class="footer__info">
      <p>ShipShip ApS</p>
      <p>CVR nr. 38937901</p>
      <p>Tlf. nr. +45 2868 1560</p>
      <a href="mailto:mail@shipship.dk">mail@shipship.dk</a>
  </div>
</div>