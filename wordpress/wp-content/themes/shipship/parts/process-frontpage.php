<div class="process" itemscope itemtype="http://schema.org/ItemList">
  <div class="process__header">
    <?php $process_section_title = carbon_get_post_meta($post->ID, 'process_section_title');  ?>
    <?php if($process_section_title) : ?>
      <h1 class="process__header--title"><?php echo $process_section_title ?></h1>
    <?php endif; ?>
    <?php $process_section_text = carbon_get_post_meta($post->ID, 'process_section_text');  ?>
    <?php if($process_section_text) : ?>
      <h2 class="process__header--description"><?php echo $process_section_text ?></h2>
    <?php endif; ?>
  </div>
  <div class="process__content">
    <?php $processes = carbon_get_post_meta($post->ID, 'process_group'); ?>
    <?php foreach ($processes as $process): ?>
      <div class="process-item" itemprop="itemListElement">
        <div class="process-item__logo" itemprop="image">
          <img src="<?php echo $process['process_illustration']; ?>" alt="">
        </div>
        <h6 class="process-item__name" itemprop="name"><?php echo $process['process_title']; ?></h6>
        <p class="process-item__description"><?php echo $process['process_text']; ?></p>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="process__link">
    <?php $process_cta = carbon_get_post_meta($post->ID, 'process_cta');  ?>
    <?php if($process_cta) : ?>
      <?php $page = get_page_by_path( 'Vores Services' ); ?>
      <a class="process__link--cta" href="<?php echo get_permalink( $page ); ?>"><?php echo $process_cta ?></a>
    <?php endif; ?>
  </div>
</div>