<?php /* Template Name: Services Template */ ?>

<?php get_header(); ?>

<?php $integrations = carbon_get_post_meta($post->ID, 'services_group'); ?>
<?php if($integrations): ?>
  <?php include_once(get_theme_file_path('parts/services.php')); ?>
<?php endif; ?>
<?php include_once(get_theme_file_path('parts/contact-section.php')); ?>
<?php get_footer(); ?>