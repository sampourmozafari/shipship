<?php /* Template Name: Integrationer Template */ ?>

<?php get_header(); ?>

<?php $integrations = carbon_get_post_meta($post->ID, 'integrations_group'); ?>
<?php if($integrations): ?>
  <?php include_once(get_theme_file_path('parts/integrations.php')); ?>
<?php endif; ?>
<?php include_once(get_theme_file_path('parts/contact-section.php')); ?>
<?php get_footer(); ?>