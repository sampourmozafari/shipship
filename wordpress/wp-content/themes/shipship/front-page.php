<?php get_header(); ?>

	<?php include('parts/process-frontpage.php'); ?>
	<?php include('parts/contact-section.php'); ?>
	<div id="faqBot">
		<iframe
			width="350"
			height="430"
			src="https://console.dialogflow.com/api-client/demo/embedded/58859156-86de-4332-a97a-b809e3c38a7b">
		</iframe>
		<span id="faqBotClose">
			<i class="fas fa-times"></i>
		</span>	
	</div>
	<div id="faqBotButton"><i class="fas fa-robot"></i></div>

<?php get_footer(); ?>