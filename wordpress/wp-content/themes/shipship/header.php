<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">
		<title><?php echo get_bloginfo( 'name' ); ?></title>
		<?php wp_head(); ?>
	</head>
	<body id="site" <?php body_class('site'); ?>>
		<div id="site-navigation" class="site-navigation">
			<?php include('parts/navigation.php'); ?>
		</div>
		<div id="site-header" class="site-header">
			<?php if(is_front_page()) : ?>
				<?php include('parts/header-frontpage.php'); ?>
			<?php else : ?>
				<?php include('parts/header-page.php'); ?>
			<?php endif; ?>
		</div>
		<div id="site-content" class="site-content">