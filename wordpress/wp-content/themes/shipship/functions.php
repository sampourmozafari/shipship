<?php

//CARBON FIELDS
use Carbon_Fields\Container;
use Carbon_Fields\Field;

//CARBON FIELDS - DEFAULT HEADER
add_action('carbon_fields_register_fields', 'page_header');
function page_header() {
    container::make( 'post_meta', 'Side header' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_id', '!=', get_option( 'page_on_front' ) )
    ->add_fields(array(
        Field::make( 'text', 'page_header_heading_title', 'Header titel'),
        // ->set_required( true ),
        Field::make( 'text', 'page_header_heading_subtitle', 'Header undertitel'),
        // ->set_required( true ),
        Field::make( 'text', 'page_contact_button_text', 'Tekst til call to action knap' )
        // ->set_required( true )
    ));
}

//CARBON FIELDS - FRONTPAGE HEADER
add_action('carbon_fields_register_fields', 'frontpage_header');
function frontpage_header() {
    container::make( 'post_meta', 'Forside header' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ) )
    ->add_fields(array(
        Field::make( 'image', 'frontpage_header_image', 'Header illustration til forsiden' )
        ->set_value_type( 'url' ),
        // ->set_required( true ),
        Field::make( 'text', 'frontpage_header_heading_title', 'Header titel'),
        // ->set_required( true ),
        Field::make( 'text', 'frontpage_header_heading_subtitle', 'Header undertitel'),
        // ->set_required( true ),
        Field::make( 'text', 'frontpage_contact_button_text', 'Tekst til call to action knap' )
        // ->set_required( true )
    ));
}

//CARBON FIELDS - FRONTPAGE THE PROCESS
add_action('carbon_fields_register_fields', 'frontpage_the_process');
function frontpage_the_process() {

    Container::make( 'post_meta', 'Processen' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ) )
    ->add_fields(array(
        Field::make( 'text', 'process_section_title', 'Titel på process sektion' ),
        // ->set_required( true ),
        Field::make( 'text', 'process_section_text', 'Tekst content til process sektion'),
        // ->set_required( true ),
        Field::make( 'complex', 'process_group', 'Process' )
        ->set_layout( 'tabbed-vertical' )
        ->add_fields(array(
            Field::make( 'image', 'process_illustration', 'Illustration til process' )
            ->set_value_type( 'url' ),
            // ->set_required( true ),
            Field::make( 'text', 'process_title', 'Titel på process' ),
            // ->set_required( true ),
            Field::make( 'text', 'process_text', 'Tekst content til process' )
            // ->set_required( true ),
        )),
        Field::make( 'text', 'process_cta', 'Tekst til call to action knap' )
    ));
}

//CARBON FIELDS - INTEGRATIONS
add_action('carbon_fields_register_fields', 'integrations');
function integrations() {
    Container::make( 'post_meta', 'Integrationer' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_template', '=', 'templates/template-integrationer.php' )
    ->add_fields(array(
        Field::make( 'complex', 'integrations_group', 'Integration' )
        ->set_layout( 'tabbed-vertical' )
        ->add_fields(array(
            Field::make( 'image', 'integration_logo', 'Logo' )
            ->set_value_type( 'url' ),
            Field::make( 'text', 'integration_title', 'Navn på integration' )
            // ->set_required( true )
        ))
    ));
}

//CARBON FIELDS - SERVICES
add_action('carbon_fields_register_fields', 'services');
function services() {
    Container:: make( 'post_meta', 'Services' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_template', '=', 'templates/template-services.php' )
    ->add_fields(array(
        Field::make( 'complex', 'services_group', 'Service' )
        ->set_layout( 'tabbed-vertical' )
        ->add_fields(array(
            Field::make( 'image', 'service_icon', 'Ikon' )
            ->set_value_type( 'url' ),
            Field::make( 'text', 'service_title', 'Navn på Service' ),
            // ->set_required( true )
            Field::make( 'text', 'service_description', 'Beskrivelse af service' )
            // ->set_required( true )
        ))
    ));
}

//CARBON FIELDS - ABOUT SHIPSHIP
add_action('carbon_fields_register_fields', 'about');
function about() {
    container::make( 'post_meta', 'Om ShipShip' )
    ->show_on_page( 'Om ShipShip' )
    ->add_fields(array(
        // Field::make( 'text', 'about_section_title', 'Titel til Om ShipShip'),
        // ->set_required( true ),
        Field::make( 'text', 'about_section_text', 'Tekst til Om ShipShip' ),
        // ->set_required( true )
    ));
}

//CARBON FIELDS - CONTACT SECTION
add_action('carbon_fields_register_fields', 'contact_section');
function contact_section() {
    $contact_page = get_page_by_path( 'Kontakt' );
    container::make( 'post_meta', 'Kontaktsektion' )
    ->where( 'post_type', '=', 'page' )
    ->Where( 'post_id', '!=', (int) $contact_page->ID )
    ->add_fields(array(
        Field::make( 'text', 'contact_section_title', 'Titel til kontaktsektion'),
        Field::make( 'text', 'contact_section_text', 'Tekst til kontaktsektion' ),
        Field::make( 'text', 'contact_section_cta', 'Tekst til call to action knap' )
    ));
}

//CARBON FIELDS - CONTACT PAGE
add_action('carbon_fields_register_fields', 'contact_page');
function contact_page() {
    $contact_page = get_page_by_path( 'Kontakt' );
    container::make( 'post_meta', 'Kontakt' )
    ->where( 'post_type', '=', 'page' )
    ->Where( 'post_id', '=', (int) $contact_page->ID )
    ->add_fields(array(
        Field::make( 'text', 'contact_page_text', 'Tekst til Kontakt'),
        Field::make( 'text', 'contact_page_phone', 'Telefon'),
        Field::make( 'text', 'contact_page_email', 'Email' ),
        Field::make( 'text', 'contact_page_name', 'Firma' ),
        Field::make( 'text', 'contact_page_cvr', 'CVR' )
    ));
}

//LOAD CARBON FIELDS
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}

//REGISTER MENUS
function register_my_menus() {
    register_nav_menus( array(
        'navigation-menu' => __( 'Hovedmenu' ),
        'footer-menu' => __( 'Footermenu' )
    ));
}
add_action( 'init', 'register_my_menus' );

//ENQUEUE PROJECT-STYLES.SCSS
function theme_styles() {
    wp_enqueue_style( 'project-styles', get_stylesheet_directory_uri() . '/build/css/project-styles.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

//ENQUEUE PROJECT-SCRIPT.JS
function theme_script() {
    wp_enqueue_script( 'project-script', get_stylesheet_directory_uri() . '/build/js/project-script.js' );
}
add_action( 'wp_enqueue_scripts', 'theme_script' );

//ENQUEUE GOOGLE FONT
function add_google_font() {
    wp_enqueue_style( 'google_font', 'https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,500,600', false );
}
add_action( 'wp_enqueue_scripts', 'add_google_font' );

//ENQUEUE FONT AWESOME
function add_font_awesome(){
	wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css', false); 
}
add_action('wp_enqueue_scripts','add_font_awesome');

//ALLOW SVG
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');